#!/bin/bash
_os="`uname`"
_now=$(date +"%m-%d-%Y")
_file="database-data/theoutdoortrip-db_$_now.sql"
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

# Export dump
EXPORT_COMMAND='exec mysqldump "$MYSQL_DATABASE" -uroot -p"$MYSQL_ROOT_PASSWORD"'
printf "${YELLOW}Please wait while the database is exporting...\n${NC}"
docker-compose exec db sh -c "$EXPORT_COMMAND" > $_file
printf "${GREEN}Database dump exported succesfully.\nYou can find it at ${YELLOW}'database-data' ${GREEN}directory. ${NC}\n" 
if [[ $_os == "Darwin"* ]] ; then
	  sed -i '.bak' 1,1d $_file
  else
	    sed -i 1,1d $_file # Removes the password warning from the file
    fi

