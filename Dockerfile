FROM wordpress:latest

LABEL Rony Almendarez <rony.almendarez03@gmail.com>

RUN apt-get update && \
	apt-get install -y  --no-install-recommends ssl-cert && \
	rm -r /var/lib/apt/lists/* && \
	a2enmod ssl && \
	a2ensite default-ssl
